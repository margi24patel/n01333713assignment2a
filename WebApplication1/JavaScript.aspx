﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="WebApplication1.WebForm1" %>


<asp:Content ID="headingJS" ContentPlaceHolderID="Heading" runat="server">
    <h1 style="text-align:center">JavaScript</h1>
        
</asp:Content>

<asp:Content ID="ilearnedJS" ContentPlaceHolderID="MainContent" runat="server">
    
    <h3>An Array</h3>
    <p>An array can hold more than one value at a time.</p>
     <p>In array, Index Number starts from [0].</p>
    <p>For Example, if you want to loop through the books to find a specific one? And what if you had not 3 books, but 100 or more? </p>

        <p> Solution: An Array</p>

        <p> array can hold many values under a single name, and you can access the values by referring to an index number.</p>
        

     <h4>Example:</h4>
    
    <%--<p>var book = ["Ramayana", "3Idiots", "Wings On Fire"];</p>--%>




   <%-- DataGrid for An Array--%>
   <%-- <asp:DataGrid ID="Array_js" runat="server" CssClass="code" GridLines="None" Width="500px"
        CellPadding="2">

        <HeaderStyle Font-Size="Large" BackColor="#ff9900" ForeColor="#ffffff" />
        <ItemStyle BackColor="#333300" ForeColor="#ffffff" />
   
    </asp:DataGrid>--%>

    <uctrl:usercode ID="Teacher_Side_Js_Code" runat="server" SkinId="GridViewSkin" code="JS" 
        owner="Teacher"></uctrl:usercode>
      
</asp:Content>

<asp:Content ID="iwroteJS" ContentPlaceHolderID="MyCode" runat="server">
    <h3>Example</h3>
    <%--<h4>Code:</h4>
   <p>
  </p>--%>


    <%-- DataGrid for JS Example--%>

    <%--<asp:DataGrid ID="Code_js" runat="server" CssClass="code" GridLines="None" Width="500px"
        CellPadding="2">

        <HeaderStyle Font-Size="Large" BackColor="#ff9900" ForeColor="#ffffff" />
        <ItemStyle BackColor="#333300" ForeColor="#ffffff" />
   
    </asp:DataGrid>--%>
    
    <uctrl:usercode ID="My_Side_Js_Code" runat="server" SkinId="GridViewSkin" code="JS" 
        owner="Me"></uctrl:usercode>
 
    <h4>Description:</h4>
    <p>Here, are three Books of Arrray.Which asks the user to pick number between 1 to 3.<br />
        The user will see  a Book Number and Book name according to number which is entered.<br />
        And also will see the Book Number and Book name on the console.<br />
        If user  click on cancel or enters a number other than 1 to 3, user will get a pop up message that says "please enter a number between 1 to 3!" 
         </p>

    
</asp:Content>


<asp:Content ID="ifoundJS" ContentPlaceHolderID="ExampleCode" runat="server">
    <h3>Array Example</h3>
    <h4>Code:</h4>
    <p>var fruits = ["Banana", "Orange", "Apple", "Mango"]; <br />
        fruits.length;

    </p>

    <h4>Description:</h4>
    <p>Here,a variable fruits contains an array of fruit name elements. <br />
        And fruits.length function will returns the length of array(fruits). 
    </p>

</asp:Content>

<asp:Content ID="listoflinkJS" ContentPlaceHolderID="Link" runat="server">

    <h3>Here, Some Helpful Links:</h3>
    <ul>
    <li><a href="https://www.w3schools.com/js/js_arrays.asp">www.W3Schools.com</a></li>
    <li><a href=" https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Arrays">MDN Web Docs</a></li>
    </ul>
</asp:Content>
